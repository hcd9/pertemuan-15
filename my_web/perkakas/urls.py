"""my_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from.views import index, AlatDetailView, AlatCreateView
from.views import AlatEditView

urlpatterns = [
    path('', index, name='home_page'),
    path('peralatan/<int:pk>', AlatDetailView.as_view(),
    name = 'alat_detail_view'),
    path('peralatan/add', AlatCreateView.as_view(), name='alat_add'),
    path('peralatan/edit/<int:pk>', AlatEditView.as_view(), name='alat_edit')
]