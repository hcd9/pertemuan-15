from .models import Peralatan
from django.shortcuts import render
from django.views.generic import DetailView, CreateView

# Create your views here.
var = {
    'judul' : 'Toko Pertukangan Aneka Kerja',
    'info' : '''Kami menyediakan segala peralatan dan perkakas pertukangan Anda, baik untuk hobi maupun profesi Anda''',
    'oleh' : 'owner'
    
}
def index(self):
    var['peralatan'] = Peralatan.objects.values('id','nama_alat','kategori').\
    order_by ('nama_alat')
    return render(self, 'perkakas/index.html', context=var)

class AlatDetailView(DetailView):
    model = Peralatan
    template_name = 'perkakas/alat_detail_view.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AlatCreateView(CreateView): #new
    model = Peralatan
    fields = '__all__'
    template_name = 'perkakas/alat_add.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

from django.views.generic import UpdateView # new
class AlatEditView(UpdateView):
    model = Peralatan
    fields = ['nama_alat', 'kategori', 'keterangan', 'berat', 'harga', 'jumlah']
    template_name = 'perkakas/alat_edit.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

    from django.urls import reverse_lazy
    from django.views.generic import DeleteView

    class AlatDeleteView(DeleteView):
        model = Peralatan
        template_name = 'perkakas/alat_delete.html'

        def get_context_data(self, **kwargs):
            context = var
            context.update(super().get_context_data(**kwargs))
            return context